import { CfnDBCluster, CfnDBInstance } from '@aws-cdk/aws-docdb';
import { Construct, Stack, StackProps } from '@aws-cdk/core';

export default class AmazonDocumentdbStack extends Stack {
  public constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const dbIdentifier = 'docdb-cdk';

    const cluster = new CfnDBCluster(this, 'DocumentDBCluster', {
      dbClusterIdentifier: dbIdentifier,
      masterUsername: 'test',
      masterUserPassword: process.env.DOCDB_MASTER_USER_PASSWORD,
    });

    const instance = new CfnDBInstance(this, 'DocumentDBInstance', {
      dbClusterIdentifier: dbIdentifier,
      dbInstanceClass: 'db.r5.large',
      dbInstanceIdentifier: dbIdentifier,
    });

    instance.addDependsOn(cluster);
  }
}
