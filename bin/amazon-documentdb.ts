#!/usr/bin/env node
import 'source-map-support/register';
import { App } from '@aws-cdk/core';
import AmazonDocumentdbStack from '../lib/amazon-documentdb-stack';

const app = new App();
new AmazonDocumentdbStack(app, 'AmazonDocumentdbStack');
